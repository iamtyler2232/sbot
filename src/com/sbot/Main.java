package com.sbot;

import com.sbot.gui.LoadingScreen;
import com.sbot.gui.Logger;
import com.sbot.hooks.Canvas;
import com.sbot.hooks.PaintListener;
import com.sbot.hooks.Skill;
import com.sbot.hooks.Skills;
import com.sbot.loader.RSLoader;
import com.sbot.utils.HookLoader;
import com.sbot.wrappers.Client;
import com.sbot.wrappers.Players;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.awt.*;
import java.io.IOException;

/**
 * Created by Tyler on 8/17/2014.
 */
public class Main {
    public static void main(String[] args) {
        //LoadingScreen loadingScreen = new LoadingScreen();



        try {
            HookLoader.test();
            //System.out.println(HookLoader.fields.get("Canvas").get("Canvas")[0].toString());
            new RSLoader();
            Canvas.addPaintListener(new PaintListener() {

                @Override
                public void paint(Graphics2D g) {
                    if(!RSLoader.hidePaint) {
                        onRepaint(g);
                    }
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    //START: Code generated using Enfilade's Easel
    private final static Color color1 = new Color(102, 102, 0);
    private final static Color color2 = new Color(0, 0, 0);
    private final static Color color3 = new Color(255, 255, 255);
    private final static Color color4 = new Color(153, 0, 153);

    private final static BasicStroke stroke1 = new BasicStroke(3);

    private final static Font font1 = new Font("Arial", 1, 12);
    private final static Font font2 = new Font("Arial", 1, 10);
    private final static Font font3 = new Font("Arial", 1, 7);
    private final static Font font4 = new Font("Arial", 1, 8);

    public static void onRepaint(Graphics g1) {
        Graphics2D g = (Graphics2D)g1;
        g.setColor(color1);
        g.fillRect(17, 16, 127, 212);
        g.setColor(color2);
        g.setStroke(stroke1);
        g.drawRect(17, 16, 127, 212);
        g.setFont(font1);
        g.setColor(color3);
        g.drawString("Player Stats", 47, 31);
        g.setColor(color4);
        g.fillRect(145, 16, 132, 211);
        g.setColor(color2);
        g.drawRect(145, 16, 132, 211);
        g.setFont(font2);
        g.setColor(color3);
        try {
            g.drawString("Animation: " + Players.getLocal().getAnimation(), 22, 54);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        g.drawString("Name: n/a", 22, 65);
        try {
            g.drawString("X: " + Players.getLocal().getX(), 22, 76);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        try {
            g.drawString("Y:  " + Players.getLocal().getY(), 22, 86);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        g.drawString("isLoggedIn: " +( Client.getLoginState() == 30), 22, 220);
        g.drawString("Game State: " + Client.getLoginState(), 22, 207);
        g.setFont(font1);
        g.drawString("Skills", 198, 30);
        g.setFont(font3);
        g.drawString("Attack:", 151, 46);
        try {
            g.drawString("Exp: "+Skills.getExperience(Skill.ATTACK), 163, 58);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        g.setFont(font4);
        try {
            g.drawString("realLevel: "+Skills.getRealLevel(Skill.ATTACK), 164, 68);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        g.drawString("boostedLevel:", 164, 77);
    }

    //END: Code generated using Enfilade's Easel
}
