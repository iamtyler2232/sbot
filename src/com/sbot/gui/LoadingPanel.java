package com.sbot.gui;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Zach on 8/20/2014.
 */
public class LoadingPanel extends JPanel{
    private JLabel title;
    private JProgressBar progressBar;

    public LoadingPanel(String panelLable){
        this.setBackground(Color.DARK_GRAY);
        this.setLayout(new FlowLayout());

        progressBar = new JProgressBar();
        progressBar.setValue(0);
        progressBar.setStringPainted(true);
        UIManager.put("nimbusOrange", new Color(40, 209, 232));//change the color of the progress bar

        title = new JLabel(panelLable);
        title.setForeground(Color.CYAN);

        this.add(title);
        this.add(progressBar);
    }

    public JProgressBar getProgressBar(){
        return  this.progressBar;
    }

}
