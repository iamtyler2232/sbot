package com.sbot.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.RoundRectangle2D;

/**
 * Created by Zach on 8/20/2014.
 */
public class LoadingScreen extends JFrame{
    private LoadingPanel loadingPanel;

    public LoadingScreen(String panelLable){
        loadingPanel = new LoadingPanel(panelLable);

        this.setSize(375, 30);
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation((dimension.width/2) - (this.getWidth()/2) , (dimension.height/2) - (this.getHeight()/2));
        this.setUndecorated(true);
        this.setShape(new RoundRectangle2D.Double(0, 0, 375, 30, 5, 5));
        this.setBackground(Color.DARK_GRAY);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.add(loadingPanel);
        this.setVisible(true);
    }

    public JPanel getLoadingPanel(){
        return this.loadingPanel;
    }

    public JProgressBar getProgressBar(){
        return this.loadingPanel.getProgressBar();
    }

}
