package com.sbot.gui;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Tyler on 8/17/2014.
 */
public class Logger extends JScrollPane {
    JScrollPane sp;
    private static JList list;
    public Logger() {
        list = new JList<>(new DefaultListModel());
        list.setBackground(Color.DARK_GRAY);
        list.setForeground(Color.CYAN);
        setViewportView(list);
    }

    public static void log(String msg) {
        DefaultListModel x = new DefaultListModel();

        for(int i = 0; i < list.getModel().getSize(); i++) {
            x.addElement(list.getModel().getElementAt(i));
        }
        x.addElement(msg);
        list.setModel(x);
        System.out.println(msg);
    }
}
