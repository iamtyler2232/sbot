package com.sbot.hooks;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

/**
 * Created by Tyler on 8/17/2014.
 */
public class Canvas extends java.awt.Canvas {
    private BufferedImage gameBuffer = new BufferedImage(765, 503, BufferedImage.TYPE_INT_RGB);
    private static ArrayList<PaintListener> paintListeners = new ArrayList<>();

    @Override
    public Graphics getGraphics() {
        final Graphics2D g = (Graphics2D) gameBuffer.getGraphics();
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        for (PaintListener paintListener : paintListeners) {
            paintListener.paint((Graphics2D) g);
        }
        super.getGraphics().drawImage(gameBuffer, 0, 0, null);
        return g;
    }

    public static void addPaintListener(PaintListener paintListener) {
        paintListeners.add(paintListener);
    }
    public void removePaintListener(PaintListener l) {
        if(paintListeners.contains(l)) paintListeners.remove(l);
    }
}

