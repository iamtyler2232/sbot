package com.sbot.hooks;

import com.sbot.utils.HookLoader;

/**
 * Created by Tyler on 8/17/2014.
 */
public class Skills extends HookLoader {
    public static int getExperience(Skill skill) throws IllegalAccessException, ClassNotFoundException {
        return getIntArray("Client","experiences")[skill.getIndex()];
    }

    public static int getRealLevel(Skill skill) throws IllegalAccessException, ClassNotFoundException {
        return getIntArray("Client","realLevels")[skill.getIndex()];
    }
}
