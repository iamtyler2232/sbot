package com.sbot.loader;

import com.sbot.gui.LoadingScreen;
import com.sbot.gui.Logger;
import jdk.internal.org.objectweb.asm.ClassReader;
import jdk.internal.org.objectweb.asm.ClassWriter;
import jdk.internal.org.objectweb.asm.Opcodes;
import jdk.internal.org.objectweb.asm.tree.AbstractInsnNode;
import jdk.internal.org.objectweb.asm.tree.ClassNode;
import jdk.internal.org.objectweb.asm.tree.MethodInsnNode;
import jdk.internal.org.objectweb.asm.tree.MethodNode;

import javax.swing.*;
import javax.swing.plaf.basic.BasicMenuBarUI;
import javax.swing.plaf.nimbus.NimbusLookAndFeel;
import java.applet.Applet;
import java.applet.AppletContext;
import java.applet.AppletStub;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarOutputStream;

/**
 * Created by Tyler on 8/17/2014.
 */
public class RSLoader extends JFrame implements AppletStub {
    public static Class<?> newInstance = null;
    public static URLClassLoader clientLoader;
    public static boolean hidePaint = false;
    public static URL gameUrl;
    public String MClass;
    public static final HashMap<String, String> Parameters = new HashMap<String, String>();
    private int status = 0;
    private final int DOWNLOADING = 1;
    Logger logger;
    private final double VERSION = 0.1;
    private LoadingScreen loadingScreen;

    public RSLoader() throws MalformedURLException, ClassNotFoundException, IllegalAccessException, InstantiationException {
        //GUI Setup


        JPopupMenu.setDefaultLightWeightPopupEnabled(false);
        try {
            UIManager.setLookAndFeel(new NimbusLookAndFeel());
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }
        logger = new Logger();
        initComponents();
        panel2.add(logger);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(770, 650);
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation((dimension.width/2) - (this.getWidth()/2) , (dimension.height/2) - (this.getHeight()/2));
        setResizable(false);

        //Get the GAME URL
        try {
            gameUrl = new URL("http://oldschool45.runescape.com/");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        try {
            getParams(gameUrl);
        } catch (IOException e) {
            e.printStackTrace();
        }
        clientLoader = new URLClassLoader(new URL[] {gameUrl});
        Logger.log("Injecting Canvas...");
        try {
            //If we dont have a client stored we gotta download/inject a new one
            File f = new File(System.getProperty("user.home"), "client.jar");
            if(f == null || !f.exists())
                injectCanvas(getJar());
        } catch (IOException e) {
            e.printStackTrace();
        }

        File file  = new File(System.getProperty("user.home"), "client.jar");
        URL url = file.toURI().toURL();
        URL[] urls = new URL[]{url};
        System.out.println(url.getHost() + url.getPath());
        clientLoader = new URLClassLoader(urls);
        Logger.log("Done");

        Logger.log("Loading client... ");
        Class<?> clazz = clientLoader.loadClass("client");
        newInstance = clazz;
        final Applet loader = (Applet) clazz.newInstance();

        loader.setStub(this);
        loader.init();
        loader.start();


        //Some final GUI stuff
        JPopupMenu.setDefaultLightWeightPopupEnabled(false); ToolTipManager.sharedInstance().setLightWeightPopupEnabled(false);

        setResizable(false);

        setTitle("SBot - V"+getVersion());
        JPopupMenu.setDefaultLightWeightPopupEnabled(false);
        JPanel theGame = new JPanel(new BorderLayout());
        theGame.setPreferredSize(new Dimension(768, 560));
        panel1.add(loader);
        setVisible(true);
        Logger.log(" Done!");
    }

    JarFile getJar() throws IOException {
        InputStream inStream = clientLoader.getURLs()[0].openStream();
        Logger.log(""+inStream.available());
        BufferedInputStream bufIn = new BufferedInputStream(inStream);
        loadingScreen = new LoadingScreen("Downloading Runescape.jar...");
        int ch = 0;
//        do{
//            ch = progressMonitorInputStream.read();
//            System.out.println(ch);
//        }while(ch != -1);

        File fileWrite = new File("client.jar");
        OutputStream out= new FileOutputStream(fileWrite);
        BufferedOutputStream bufOut = new BufferedOutputStream(out);
        float totalSize = 1496914, currentlyDownloadedBytes = 0;
        float progressPercent = 0;
        byte buffer[] = new byte[2048];
        while (true) {
            int nRead = bufIn.read(buffer, 0, buffer.length);
            currentlyDownloadedBytes += nRead;
            progressPercent = currentlyDownloadedBytes/totalSize;
            if(progressPercent <= 1){
                loadingScreen.getProgressBar().setValue((int)(100*progressPercent));
            }
            if (nRead <= 0)
                break;
            bufOut.write(buffer, 0, nRead);
        }

        loadingScreen.dispose();

        bufOut.flush();
        out.close();
        inStream.close();
        return new JarFile(fileWrite);
    }

    public double getVersion() {
        return this.VERSION;
    }

    public void getParams(URL url) throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
        String line;
        List<String> params = new ArrayList<String>();
        while((line = reader.readLine()) != null)
        {
            if(line.contains("param name"))
                params.add(line);
            if(line.contains("archive"))
                gameUrl = new URL("http://oldschool1.runescape.com/" + line.substring(line.indexOf("archive=") + 8, line.indexOf(" ');")));

            if(MClass == null)
                if(line.contains("code="))
                    MClass = line.substring(line.indexOf("code=") + 5, line.indexOf(".class"));
        }
        reader.close();

        for(String s : params)
        {
            Parameters.put(getParamName(s), getParamValue(s));
        }
    }
    public String getParamName(String param)
    {
        try{
            int StartIndex = param.indexOf("<param name=\"") + 13;
            int EndIndex = param.indexOf("\" value");
            return param.substring(StartIndex, EndIndex);
        }catch(StringIndexOutOfBoundsException e)//classic handles some differently so why not just catch it =P
        {
            int StartIndex = param.indexOf("<param name=") + 12;
            int EndIndex = param.indexOf(" value");
            return param.substring(StartIndex, EndIndex);
        }
    }

    public String getParamValue(String param)
    {
        try{
            int StartIndex = param.indexOf("value=\"") + 7;
            int EndIndex = param.indexOf("\">');");
            return param.substring(StartIndex, EndIndex);
        }catch(StringIndexOutOfBoundsException e)//and again :D
        {
            int StartIndex = param.indexOf("value=") + 6;
            int EndIndex = param.indexOf(">');");
            return param.substring(StartIndex, EndIndex);
        }
    }

    public static void injectCanvas(JarFile jarFile) {
        parseJar(jarFile);
        save();
    }

    static HashMap<String,ClassNode> classes = new HashMap<>();

    private static void parseJar(JarFile jar) {
        try {
            Enumeration<?> enumeration = jar.entries();
            while (enumeration.hasMoreElements()) {
                JarEntry entry = (JarEntry) enumeration.nextElement();
                if (entry.getName().endsWith(".class")) {
                    ClassReader cr = new ClassReader(jar.getInputStream(entry));
                    ClassNode cn = new ClassNode();
                    cr.accept(cn, ClassReader.SKIP_DEBUG | ClassReader.SKIP_FRAMES);
                    if(cn.superName.contains("Canvas")) {
                        setSuper(cn, "com/sbot/hooks/Canvas");
                        Logger.log("Overrode canvas");
                    }
                    classes.put(cn.name, cn);
                }
            }
            jar.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unchecked")
    public static void setSuper(ClassNode node, String superClass) {
        node.superName = superClass;
        for (Object method : node.methods) {
            MethodNode mn = (MethodNode) method;
            if (mn.name.equals("<init>")) {
                ListIterator<?> ili = mn.instructions.iterator();
                while (ili.hasNext()) {
                    AbstractInsnNode ain = (AbstractInsnNode) ili.next();
                    if (ain.getOpcode() == Opcodes.INVOKESPECIAL) {
                        ((MethodInsnNode) ain).owner = superClass;
                        break;
                    }
                }
            }
        }
    }

    public static void save() {
        File injected = new File(System.getProperty("user.home"), "client.jar");
        try (JarOutputStream out = new JarOutputStream(new FileOutputStream(injected))) {
            for (ClassNode classNode : classes.values()) {
                out.putNextEntry(new JarEntry(classNode.name + ".class"));
                writeClass(classNode, out);
            }
            out.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private static void writeClass(ClassNode classNode, JarOutputStream out) throws IOException {
        ClassWriter writer = new ClassWriter(ClassWriter.COMPUTE_MAXS);
        classNode.accept(writer);
        out.write(writer.toByteArray());
    }

    @Override
    public URL getCodeBase() {
        try
        {
            return new URL("http://oldschool1.runescape.com/");
        }catch(MalformedURLException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public URL getDocumentBase() {
        try
        {
            return new URL("http://oldschool1.runescape.com/");
        }catch(MalformedURLException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String getParameter(String arg0) {
        return Parameters.get(arg0);
    }

    @Override
    public AppletContext getAppletContext() {
        return null;
    }

    @Override
    public void appletResize(int width, int height) {

    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - QBots Ftw
        menuBar1 = new JMenuBar();
        panel1 = new JPanel();
        panel2 = new JPanel();
        JMenu menu1 = new JMenu();
        JMenuItem menuItem1=new JMenuItem(),menuItem2=new JMenuItem();
        JButton label1 = new JButton();



        //======== this ========
        Container contentPane = getContentPane();
        contentPane.setLayout(null);

        {

            //======== menu1 ========
            {
                menu1.setText("Script");
                menu1.setActionCommand("Script");

                //---- menuItem1 ----
                menuItem1.setText("Start");
                menu1.add(menuItem1);

                //---- menuItem2 ----
                menuItem2.setText("Stop");
                menu1.add(menuItem2);
            }
            menuBar1.add(menu1);

            //---- label1 ----
            label1.setText("Toggle Paint");
            label1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    //printLevels(e);
                    hidePaint = !hidePaint;
                }
            });
            menuBar1.add(label1);
        }

        menuBar1.setUI ( new BasicMenuBarUI(){
            public void paint ( Graphics g, JComponent c ){
                g.setColor ( Color.DARK_GRAY );
                g.fillRect ( 0, 0, c.getWidth (), c.getHeight () );
            }
        } );
        menu1.setForeground(Color.CYAN);

        setJMenuBar(menuBar1);


        //======== panel1 ========
        {
            panel1.setLayout(new BorderLayout());
        }
        panel1.setLocation(0,0);
        panel1.setSize(768,500);
        contentPane.add(panel1);

        //======== panel2 ========
        {
            panel2.setLayout(new BorderLayout());
        }
        panel2.setSize(768,110);
        panel2.setLocation(0,501);
        contentPane.add(panel2);
        pack();
        //setLocationRelativeTo(getOwner());
    }

    private JMenuBar menuBar1;
    private JPanel panel1;
    private JPanel panel2;
}
