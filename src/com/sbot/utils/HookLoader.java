package com.sbot.utils;

import com.sbot.loader.RSLoader;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.lang.reflect.Field;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

/**
 * Created by Tyler on 8/17/2014.
 */
public class HookLoader {
    public static HashMap<String,HashMap<String,Object[]>> fields = new HashMap<String, HashMap<String,Object[]>>();

    private static URL hooksUrl;

    /*public static boolean getHooks() throws IOException {
        hooksUrl = new URL(Settings.HOOKS_URL);
        BufferedReader in = new BufferedReader(
                new InputStreamReader(hooksUrl.openStream()));

        String inputLine;
        while ((inputLine = in.readLine())  != null) {
            String[] f = inputLine.split(" identified --> ");
            if(f[0].contains(">"))
            try {
                final HashMap<String,String> hm = new HashMap<String, String>();

                fields.put(f[0], f[1]);
            } catch(Exception e) {}
        }
        for(String name : fields.keySet()) {
            System.out.println(name+" | "+fields.get(name));
        }
        in.close();

        return false;
    }*/
    
    public static void test() throws IOException, SAXException, ParserConfigurationException {
        DocumentBuilderFactory factory =
                        DocumentBuilderFactory.newInstance();

                //Get the DOM Builder
                DocumentBuilder builder = factory.newDocumentBuilder();
        StringBuilder sb = new StringBuilder();
        try {
            // Create a URL for the desired page
            URL url = new URL("http://pastebin.com/raw.php?i=rBYWcQVK");

            // Read all the text returned by the server
            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
            //System.out.println(in.lines().toS);
            String str;
            while ((str = in.readLine()) != null) {
                //str = in.readLine().toString();
                sb.append(str);
               //System.out.println(str);
                // str is one line of text; readLine() strips the newline character(s)
            }
            in.close();
        } catch (MalformedURLException e) {
        } catch (IOException e) {
        }

        
                //Load and Parse the XML document
                //document contains the complete XML as a Tree.
                Document document =
                        builder.parse(new InputSource(new ByteArrayInputStream(sb.toString().getBytes("utf-8"))));;

        
                //Iterating through the nodes and extracting the data.
                NodeList nodeList = document.getDocumentElement().getChildNodes();
        
                for (int i = 0; i < nodeList.getLength(); i++) {

                        //We have encountered an <employee> tag.
                        org.w3c.dom.Node node = nodeList.item(i);
                        if (node instanceof Element) {
                                //System.out.println("Class: "+((Element) node).getAttribute("parent"));
                            HashMap<String,Object[]> f = new HashMap<String, Object[]>();

                                NodeList childNodes = node.getChildNodes();
                                for (int j = 0; j < childNodes.getLength(); j++) {
                                        org.w3c.dom.Node cNode = childNodes.item(j);
                    
                                        //Identifying the child tag of employee encountered.
                                        if (cNode instanceof Element) {
                                                String content = cNode.getLastChild().
                                                getTextContent().trim();
                                            String name = ((Element) cNode).getAttribute("name");
                                            if (name != null) {
                                                //System.out.println("Name = " + name);


                                            }
                                            String identified = ((Element) cNode).getElementsByTagName("identified").item(0).getTextContent();
                                            //System.out.println("    Identified: "+identified);

                                            int multi = -1;
                                            try {
                                                multi = Integer.parseInt(((Element) cNode).getElementsByTagName("multiplier").item(0).getTextContent().replace("*",""));
                                            } catch(Exception e) {}

                                            if(multi != -1)
                                                f.put(name,new Object[] {((Element) cNode).getAttribute("parent"),identified,multi});
                                            else
                                                f.put(name,new Object[] {((Element) cNode).getAttribute("parent"),identified});
                                            }
                                    }
                //empList.add(emp
                f.put(((Element) node).getAttribute("name"),new Object[] {((Element) node).getAttribute("identified")});
                fields.put(((Element) node).getAttribute("name"),f);
            }
        }

    }

    public static int[] getIntArray(String className, String methodName) {
        try {
            String classI = HookLoader.fields.get(className).get(methodName)[0].toString().toLowerCase();
            String mName = HookLoader.fields.get(className).get(methodName)[1].toString();
            int multi = 0;
            try {
                multi = Integer.parseInt(HookLoader.fields.get(className).get(methodName)[2].toString());
            } catch (Exception e) {
            }
            Field f = null;
            try {
                RSLoader.newInstance = RSLoader.clientLoader.loadClass(classI);
                f = RSLoader.newInstance.getDeclaredField(mName);
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
            if (f != null) {
                f.setAccessible(true);
                try {
                    return (int[]) f.get(RSLoader.clientLoader.loadClass(classI));
                } catch (Exception e) {
                    return (int[]) f.get(RSLoader.clientLoader.loadClass(null));
                }
            }
            return null;
        } catch(Exception e) {return null;}
    }

    public static int[] getIntArray(String className, String methodName, Object instance) {
        try {
            String classI = HookLoader.fields.get(className).get(methodName)[0].toString().toLowerCase();
            String mName = HookLoader.fields.get(className).get(methodName)[1].toString();
            int multi = 0;
            try {
                multi = Integer.parseInt(HookLoader.fields.get(className).get(methodName)[2].toString());
            } catch (Exception e) {
            }
            Field f = null;
            try {
                RSLoader.newInstance = RSLoader.clientLoader.loadClass(classI);
                f = RSLoader.newInstance.getDeclaredField(mName);
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
            if (f != null) {
                f.setAccessible(true);
                try {
                    return (int[]) f.get(instance);
                } catch (Exception e) {
                    return (int[]) f.get(RSLoader.clientLoader.loadClass(null));
                }
            }
            return null;
        } catch(Exception e) {return null;}
    }
    public static int getInt(String className, String methodName) {
        try {
            String classI = HookLoader.fields.get(className).get(methodName)[0].toString().toLowerCase();
            String mName = HookLoader.fields.get(className).get(methodName)[1].toString();

            int multi = 0;
            try {
                multi = Integer.parseInt(HookLoader.fields.get(className).get(methodName)[2].toString());
            } catch (Exception e) {
            }
            Field f = null;
            try {
                RSLoader.newInstance = RSLoader.clientLoader.loadClass(classI);
                f = RSLoader.newInstance.getDeclaredField(mName);
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
            if(f != null) {
                f.setAccessible(true);
                try {
                    return (int) f.get(RSLoader.clientLoader.loadClass(classI))*multi;
                }catch(Exception e ){
                    return (int) f.get(RSLoader.clientLoader.loadClass(null))*multi;
                }
            }
            return -1;
        }catch(Exception e) {return -1;}
    }

    public static Object getObject(String className, String methodName) {
        try {
            String classI = HookLoader.fields.get(className).get(methodName)[0].toString().toLowerCase();
            String mName = HookLoader.fields.get(className).get(methodName)[1].toString();

            int multi = 0;
            try {
                multi = Integer.parseInt(HookLoader.fields.get(className).get(methodName)[2].toString());
            } catch (Exception e) {
            }
            Field f = null;
            try {
                RSLoader.newInstance = RSLoader.clientLoader.loadClass(classI);
                f = RSLoader.newInstance.getDeclaredField(mName);
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
            if(f != null) {
                f.setAccessible(true);
                try {
                    return  f.get(RSLoader.clientLoader.loadClass(classI));
                }catch(Exception e ){
                    return  f.get(RSLoader.clientLoader.loadClass(null));
                }
            }
            return null;
        }catch(Exception e) {return null;}
    }
    public static Object getObject(String className, String methodName, Object o) {
        try {
            String classI = HookLoader.fields.get(className).get(methodName)[0].toString().toLowerCase();
            String mName = HookLoader.fields.get(className).get(methodName)[1].toString();
            int multi = 0;
            try {
                multi = Integer.parseInt(HookLoader.fields.get(className).get(methodName)[2].toString());
            } catch (Exception e) {
            }
            Field f = null;
            try {
                RSLoader.newInstance = RSLoader.clientLoader.loadClass(classI);
                f = RSLoader.newInstance.getDeclaredField(mName);
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
            if(f != null) {
                f.setAccessible(true);
                try {
                    return  f.get(o);
                }catch(Exception e ){
                    return  f.get(RSLoader.clientLoader.loadClass(null));
                }
            }
            return -1;
        }catch(Exception e) {
            return -1;}
    }

    public static int getInt(String className, String methodName, Object o) {
        try {
            String classI = HookLoader.fields.get(className).get(methodName)[0].toString().toLowerCase();
            String mName = HookLoader.fields.get(className).get(methodName)[1].toString();
            int multi = 0;
            try {
                multi = Integer.parseInt(HookLoader.fields.get(className).get(methodName)[2].toString());
            } catch (Exception e) {
            }
            Field f = null;
            try {
                RSLoader.newInstance = RSLoader.clientLoader.loadClass(classI);
                f = RSLoader.newInstance.getDeclaredField(mName);
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
            if(f != null) {
                f.setAccessible(true);
                try {
                    return (int) f.get(o)*multi;
                }catch(Exception e ){
                    return (int) f.get(RSLoader.clientLoader.loadClass(null))*multi;
                }
            }
            return -1;
        }catch(Exception e) {
            return -1;}
    }

    public static boolean getBoolean(String className, String methodName) {
        try {
            String classI = HookLoader.fields.get(className).get(methodName)[0].toString().toLowerCase();
            String mName = HookLoader.fields.get(className).get(methodName)[1].toString();
            int multi = 0;
            try {
                multi = Integer.parseInt(HookLoader.fields.get(className).get(methodName)[2].toString());
            } catch (Exception e) {
            }
            Field f = null;
            try {
                RSLoader.newInstance = RSLoader.clientLoader.loadClass(classI);
                f = RSLoader.newInstance.getDeclaredField(mName);
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
            if(f != null) {
                f.setAccessible(true);
                try {
                    return (boolean) f.get(RSLoader.clientLoader.loadClass(classI));
                }catch(Exception e ){
                    return (boolean) f.get(RSLoader.clientLoader.loadClass(null));
                }
            }
            return false;
        }catch(Exception e) {return false;}
    }

    public static boolean getBoolean(String className, String methodName, Object o) {
        try {
            String classI = HookLoader.fields.get(className).get(methodName)[0].toString().toLowerCase();
            String mName = HookLoader.fields.get(className).get(methodName)[1].toString();
            int multi = 0;
            try {
                multi = Integer.parseInt(HookLoader.fields.get(className).get(methodName)[2].toString());
            } catch (Exception e) {
            }
            Field f = null;
            try {
                RSLoader.newInstance = RSLoader.clientLoader.loadClass(classI);
                f = RSLoader.newInstance.getDeclaredField(mName);
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
            if(f != null) {
                f.setAccessible(true);
                try {
                    return (boolean) f.get(o);
                }catch(Exception e ){
                    return (boolean) f.get(RSLoader.clientLoader.loadClass(null));
                }
            }
            return false;
        }catch(Exception e) {
            return false;}
    }

    public static String getString(String className, String methodName) {
        try {
            String classI = HookLoader.fields.get(className).get(methodName)[0].toString().toLowerCase();
            String mName = HookLoader.fields.get(className).get(methodName)[1].toString();
            int multi = 0;
            try {
                multi = Integer.parseInt(HookLoader.fields.get(className).get(methodName)[2].toString());
            } catch (Exception e) {
            }
            Field f = null;
            try {
                RSLoader.newInstance = RSLoader.clientLoader.loadClass(classI);
                f = RSLoader.newInstance.getDeclaredField(mName);
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
            if(f != null) {
                f.setAccessible(true);
                try {
                    return (String) f.get(RSLoader.clientLoader.loadClass(classI));
                }catch(Exception e ){
                    return (String) f.get(RSLoader.clientLoader.loadClass(null));
                }
            }
            return null;
        }catch(Exception e) {return null;}
    }

    public static String getString(String className, String methodName, Object o) {
        try {
            String classI = HookLoader.fields.get(className).get(methodName)[0].toString().toLowerCase();
            String mName = HookLoader.fields.get(className).get(methodName)[1].toString();
            int multi = 0;
            try {
                multi = Integer.parseInt(HookLoader.fields.get(className).get(methodName)[2].toString());
            } catch (Exception e) {
            }
            Field f = null;
            try {
                RSLoader.newInstance = RSLoader.clientLoader.loadClass(classI);
                f = RSLoader.newInstance.getDeclaredField(mName);
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
            if(f != null) {
                f.setAccessible(true);
                try {
                    return (String) f.get(o);
                }catch(Exception e ){
                    return (String) f.get(RSLoader.clientLoader.loadClass(null));
                }
            }
            return null;
        }catch(Exception e) {
            return null;}
    }

    public static long getLong(String className, String methodName) {
        try {
            String classI = HookLoader.fields.get(className).get(methodName)[0].toString().toLowerCase();
            String mName = HookLoader.fields.get(className).get(methodName)[1].toString();
            int multi = 0;
            try {
                multi = Integer.parseInt(HookLoader.fields.get(className).get(methodName)[2].toString());
            } catch (Exception e) {
            }
            Field f = null;
            try {
                RSLoader.newInstance = RSLoader.clientLoader.loadClass(classI);
                f = RSLoader.newInstance.getDeclaredField(mName);
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
            if(f != null) {
                f.setAccessible(true);
                try {
                    return (long) f.get(RSLoader.clientLoader.loadClass(classI))*multi;
                }catch(Exception e ){
                    return (long) f.get(RSLoader.clientLoader.loadClass(null))*multi;
                }
            }
            return -1;
        }catch(Exception e) {return -1;}
    }

    public static long getLong(String className, String methodName, Object o) {
        try {
            String classI = HookLoader.fields.get(className).get(methodName)[0].toString().toLowerCase();
            String mName = HookLoader.fields.get(className).get(methodName)[1].toString();
            int multi = 0;
            try {
                multi = Integer.parseInt(HookLoader.fields.get(className).get(methodName)[2].toString());
            } catch (Exception e) {
            }
            Field f = null;
            try {
                RSLoader.newInstance = RSLoader.clientLoader.loadClass(classI);
                f = RSLoader.newInstance.getDeclaredField(mName);
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
            if(f != null) {
                f.setAccessible(true);
                try {
                    return (long) f.get(o)*multi;
                }catch(Exception e ){
                    return (long) f.get(RSLoader.clientLoader.loadClass(null))*multi;
                }
            }
            return -1;
        }catch(Exception e) {
            return -1;}
    }

    public static String[] getStringArray(String className, String methodName) {
        try {
            String classI = HookLoader.fields.get(className).get(methodName)[0].toString().toLowerCase();
            String mName = HookLoader.fields.get(className).get(methodName)[1].toString();
            int multi = 0;
            try {
                multi = Integer.parseInt(HookLoader.fields.get(className).get(methodName)[2].toString());
            } catch (Exception e) {
            }
            Field f = null;
            try {
                RSLoader.newInstance = RSLoader.clientLoader.loadClass(classI);
                f = RSLoader.newInstance.getDeclaredField(mName);
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
            if (f != null) {
                f.setAccessible(true);
                try {
                    return (String[]) f.get(RSLoader.clientLoader.loadClass(classI));
                } catch (Exception e) {
                    return (String[]) f.get(RSLoader.clientLoader.loadClass(null));
                }
            }
            return null;
        }catch(Exception e) {return null;}
    }

    public static String[] getStringArray(String className, String methodName, Object o) {
        try {
            String classI = HookLoader.fields.get(className).get(methodName)[0].toString().toLowerCase();
            String mName = HookLoader.fields.get(className).get(methodName)[1].toString();
            int multi = 0;
            try {
                multi = Integer.parseInt(HookLoader.fields.get(className).get(methodName)[2].toString());
            } catch (Exception e) {
            }
            Field f = null;
            try {
                RSLoader.newInstance = RSLoader.clientLoader.loadClass(classI);
                f = RSLoader.newInstance.getDeclaredField(mName);
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
            if(f != null) {
                f.setAccessible(true);
                try {
                    return (String[]) f.get(o);
                }catch(Exception e ){
                    return (String[]) f.get(RSLoader.clientLoader.loadClass(null));
                }
            }
            return null;
        }catch(Exception e) {
            return null;}
    }
}
