package com.sbot.wrappers;

import com.sbot.utils.HookLoader;

/**
 * Created by Mysteryy on 8/18/2014.
 */
public class Actor extends HookLoader{
    Object instance;

    public void setInstance(Object o) {
        instance = o;
    }

    public int getAnimation() throws IllegalAccessException, ClassNotFoundException {
        return getInt("Actor", "animation",instance);
    }

    public int getMaxHealth() throws IllegalAccessException, ClassNotFoundException {
        return getInt("Actor", "maxHealth",instance);
    }

    public int getCurrentHealth() throws IllegalAccessException, ClassNotFoundException {
        return getInt("Actor", "currentHealth",instance);
    }

    public int getX() throws IllegalAccessException, ClassNotFoundException {
        return getInt("Actor", "x",instance);
    }

    public int getY() throws IllegalAccessException, ClassNotFoundException {
        return getInt("Actor", "y",instance);
    }

    public int getPathLength() throws IllegalAccessException, ClassNotFoundException {
        return getInt("Actor", "pathLength",instance);
    }

    public boolean isMoving() throws ClassNotFoundException, IllegalAccessException {
        return getPathLength() == 0;
    }

    public boolean isAnimating() throws ClassNotFoundException, IllegalAccessException {
        return getAnimation() != -1;
    }

}
