package com.sbot.wrappers;

import com.sbot.utils.HookLoader;

/**
 * Created by Tyler on 8/19/2014.
 */
public class Client extends HookLoader {
    public static int getBaseX() {
        return getInt("Client","baseX");
    }

    public static int getBaseY() {
        return getInt("Client","baseY");
    }

    public static int getLoginState() {
        return getInt("Client","loginState");
    }

    public static Npc[] getLocalNpcs() {
        return (Npc[]) getObject("Client","localNpcs");
    }
}
