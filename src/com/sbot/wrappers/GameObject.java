package com.sbot.wrappers;


import com.sbot.utils.HookLoader;

/**
 * Created by Zach on 8/18/2014.
 */
public class GameObject extends GameObjectDefinition{

    public static int getVertexHeight(){
        return HookLoader.getInt("GameObject", "vertexHeight");
    }

    public static int getVertexHeightTop(){
        return HookLoader.getInt("GameObject", "vertexHeightTop");
    }

    public static int getId(){
        return HookLoader.getInt("GameObject", "id");
    }

    public static int getClickType(){
        return HookLoader.getInt("GameObject", "clickType");
    }

    public static int getFace(){
        return HookLoader.getInt("GameObject", "face");
    }

    public static int getConfigId(){
        return HookLoader.getInt("GameObject", "configId");
    }

}
