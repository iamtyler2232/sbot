package com.sbot.wrappers;

import com.sbot.utils.HookLoader;

/**
 * Created by Zach on 8/18/2014.
 */
public class GameObjectDefinition extends HookLoader{

    public static String getName() throws IllegalAccessException, ClassNotFoundException {
        return getString("GameObjectDefinition", "name");
    }

    public static boolean isWalkable() throws IllegalAccessException, ClassNotFoundException {
        return getBoolean("GameObjectDefinition", "walkable");
    }

    public static int getAnimationId() throws IllegalAccessException, ClassNotFoundException {
        return getInt("GameObjectDefinition", "animationId");
    }

    public static String[] getActions() throws IllegalAccessException, ClassNotFoundException {
        return getStringArray("GameObjectDefinition", "actions");
    }

}
