package com.sbot.wrappers;

import com.sbot.utils.HookLoader;

/**
 * Created by Zach on 8/18/2014.
 */
public class ItemDefinition extends HookLoader{

    public String[] getGroundActions(){
        return getStringArray("ItemDefinition", "groundActions");
    }

    public String getName(){
        return getString("ItemDefinition", "name");
    }

    public int getId(){
        return getInt("ItemDefinition", "id");
    }

    public String[] getInventoryActions(){
        return getStringArray("ItemDefinition", "inventoryActions");
    }

    public boolean isStackable(){
        return getBoolean("ItemDefinition", "stackable");
    }

    public int getItemCount(){
        return getInt("ItemDefinition", "itemCount");
    }

}
