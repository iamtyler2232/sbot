package com.sbot.wrappers;

/**
 * Created by Mysteryy on 8/18/2014.
 */
public class Npc extends Actor {
    Object instance;
    public Npc(Object instance) {
        this.instance = instance;
    }

    public NpcDefinition getDefinition() {
        NpcDefinition def = (NpcDefinition)getObject("Npc","npcDefiniton",instance);
        def.setInstance(this);
        return def;
    }
}
