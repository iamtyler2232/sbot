package com.sbot.wrappers;

import com.sbot.utils.HookLoader;

/**
 * Created by Tyler on 8/19/2014.
 */
public class NpcDefinition extends HookLoader{
    Object instance;

    public void setInstance(Object o) {
        this.instance = o;
    }

    public String[] getActions() {
        return getStringArray("NpcDefinition","getActions",instance);
    }

    public int getCombatLevel() {
        return getInt("NpcDefinition","getCombatLevel",instance);
    }

    public int getHeadIcon() {
        return getInt("NpcDefinition","getHeadIcon",instance);
    }

    public int getId() {
        return getInt("NpcDefinition","getId",instance);
    }
}
