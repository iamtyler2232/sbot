package com.sbot.wrappers;

/**
 * Created by Mysteryy on 8/18/2014.
 */
public class Player extends Actor{
    static Object inst;
    public Player(Object o) {
        inst = o;
        setInstance(o);
    }


    public int getAnimation() {
        return getInt("Actor","animation",inst);
    }

    public static int getGender(){
            return getInt("Player", "gender");
    }

    public static int getTeamId(){
            return getInt("Player", "teamId");
    }

    public static int getHeadIcon(){
            return getInt("Player", "headIcon");
    }

    public static boolean isVisible(){
            return getBoolean("Player", "visible");
    }

}
