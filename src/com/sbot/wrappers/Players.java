package com.sbot.wrappers;

import com.sbot.loader.RSLoader;
import com.sbot.utils.HookLoader;

import java.lang.reflect.Field;

/**
 * Created by Tyler on 8/19/2014.
 */
public class Players extends HookLoader{
    public static Player getLocal() throws NoSuchFieldException, IllegalAccessException, ClassNotFoundException {
        try {
                Object o = getObject("Client","localPlayer");
                //Class clazz = o.getClass();
                Player p = new Player(o);
                return p;
        } catch(NullPointerException e) { return null;}
    }
}
